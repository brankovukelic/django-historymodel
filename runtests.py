#!/usr/bin/env python

from os.path import dirname, abspath

from django.conf import settings

from nose.plugins.plugintest import run_buffered

if not settings.configured:
    settings.configure(
        ROOT_URLCONF='',
        DEBUG=False,
    )


def run(argv):
    sys.path.insert(0, dirname(abspath(__file__)))
    run_buffered(argv=argv)


if __name__ == '__main__':
    import sys
    run(sys.argv)

