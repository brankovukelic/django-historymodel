from distutils.core import setup

setup(
    name='django-historymodel',
    version='0.0.1',
    packages=['tests', 'historymodel'],
    url='https://bitbucket.org/7carrots/django-historymodel',
    license='BSD',
    author='7carrots',
    author_email='code@7carrots.com',
    description='Django pluggable app for making snapshots of model objects.'
)
